import request from '@/utils/request'
export function list() {
  return request({
    url: '/user/list',
    method: 'get'
  })
}
export function page(param) {
  return request({
    url: '/user/getByPage',
    method: 'get',
    params: param
  })
}

export function add(data){
  return request({
    url: '/user/insert',
    method: 'put',
    params: data
  })
}
export function edit(data){
  return request({
    url: '/user/update',
    method: 'post',
    params: data
  })
}
export function get(id){
  return request({
    url: '/user/get/' + id,
    method: 'get'
  })
}
export function del(id){
  return request({
    url: '/user/delete/' + id,
    method: 'delete'
  })
}
