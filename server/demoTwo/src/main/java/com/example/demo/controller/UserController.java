package com.example.demo.controller;


import com.example.demo.entity.User;
import com.example.demo.mapper.UserMapper;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.List;


@RestController
@RequestMapping("user")
public class UserController {
    @Resource
    private UserMapper userMapper;

    @PutMapping("insert")
    public String save(User user) throws Exception{
        return String.format("插入成功，返回受影响行数 %d",userMapper.save(user));
    }

    @GetMapping("list")
    public List<User> list(){
        return userMapper.selectUsers();
    }

}
